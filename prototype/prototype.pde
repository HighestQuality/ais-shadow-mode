// a template for receiving face tracking osc messages from
// Kyle McDonald's FaceOSC https://github.com/kylemcdonald/ofxFaceTracker
//
// 2012 Dan Wilcox danomatika.com
// for the IACD Spring 2012 class at the CMU School of Art
//
// adapted from from Greg Borenstein's 2011 example
// http://www.gregborenstein.com/
// https://gist.github.com/1603230
//

//Xavier Apostol
//Generative Faces: Plotter Project Concept
  FaceDetection face;
  TeddyBear teddy;

  int elapsed;
  int lastTime;
  int hungryTimer = 5000; //5 sec
  double hungryIncrease = 0.1;
  double hungryDecrease = 0.1;

  void setup() {
    //size(1200, 900, P3D);
    size(800, 600, P3D);
    frameRate(30);

    oscP5 = new OscP5(this, 8338);
    face = new FaceDetection(oscP5);
    teddy = new TeddyBear(1, this);
  }

  void draw() {  
    background(#7ACEAD);
    strokeWeight(1);
    noFill();

    if (face.found != 0) {
      teddy.drawBear(face);
    }
    
    elapsed = millis() - lastTime;
    if (elapsed >= hungryTimer && teddy.getHungry()!=1) {
      double inc = teddy.getHungry() + hungryIncrease;
      teddy.setHungry(inc);
      teddy.setHungrySoundAmp((float)inc);
      teddy.playHungrySound();
      lastTime = millis();
    }
    
    face.checkEating();
    if(face.isEating()) {
      //println("is eating");
      double dec = teddy.getHungry() - hungryDecrease;
      teddy.setHungry(dec);
      face.setEating(false);
      lastTime = millis();
    }
    
    
    //Explanation
    fill(#464E4B);    
    textSize(14);
    textAlign(CENTER);
    textMode(SHAPE);
    String explanation = "This is Charlie, he's your most favourite work tool now. Sometimes you need to take care of him."
    + "\nIf he is sad try to play with him by moving your head. If he is hungry try to feed him by imitating eating (open / close your mouth)."
    + "\nHave fun! :-)";
    text(explanation, width/2, height - 80);
    
  }

 