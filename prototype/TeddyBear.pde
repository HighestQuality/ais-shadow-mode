public class TeddyBear //<>// //<>//
{

  private int faceSize;
  private int bodySize;

  private color fur = #AD5F3B;
  private color furLight = #D47448;
  private color furLittleLight = #C76D44;
  private color furDark = #472718;
  private color furLittleDark = #874A2E;
  private int eyeDistance;
  private int mouthDistance;
  private int eyeSize;
  private int noseSize;
  private float scaling;

  private double isHungry = 0; 
  private double isSad = 0;
  
  private Sound hungerSound;

  public TeddyBear(float scaling, PApplet p)
  {
    this.scaling = scaling;
    hungerSound = new Sound(p, "hungry.mp3");

    //general size which matches to a human face
    int size = 40;
    faceSize = size;
    bodySize = (int) (size*0.9);
    noseSize = faceSize / 9;
    eyeDistance = faceSize/4;
    eyeSize = faceSize / 10;
    mouthDistance = faceSize / 4;
  }

  /* Range 0.0 - 1.0
   *  were 0 is not hungry and 1 is hungry. 
   *  please slowly decrease value while eating ("thoughts" of food will slowly disappear);
   */
  public void setHungry(double hunger)
  {
    //println("setHungry(): " + hunger);
    if (hunger < 0)
      isHungry = 0;
    if (hunger > 1)
      isHungry = 1;
    else
      isHungry = hunger;
  }
  
  public double getHungry() {
    return isHungry;
  }
  
  public void playHungrySound() {
    hungerSound.play();
  }
  
  public void setHungrySoundAmp(float amp) {
    hungerSound.setAmp(amp);
  }
  

  /* Range 0.0 - 1.0
   *  were 0 is not sad and 1 is sad. 
   *  please slowly decrease value while playing ("thoughts" of sadness will slowly disappear);
   */
  public void setSad(double sadness)
  {
    if (sadness < 0)
      isSad = 0;
    if (sadness > 1)
      isSad = 1;
    else
      isSad = sadness;
  }
  
  public double getSad() {
    return isSad;
  }

  public void drawBear(FaceDetection face)
  {
    pushMatrix();
    pushStyle();

    translate(width/2, height/2);
    scale(face.poseScale * scaling);

    drawBody();

    if (isHungry > 0 || isSad > 0)
    {
      drawNeeds();
    }

    //APPLY HEAD ROTATION
    rotateZ(0-face.poseOrientation.z);
    //rotateY(0 - face.poseOrientation.y);
    //rotateX(0 - face.poseOrientation.x);

    drawFace();

    popStyle();
    popMatrix();
  }

  private void drawBody()
  {
    //BODY
    noStroke();
    fill(furLittleDark);
    ellipse(0, faceSize/2 + bodySize/3, bodySize, bodySize);    

    //ARMS
    stroke(furLittleLight);
    fill(fur);
    ellipse(faceSize/2.5, faceSize/2, bodySize/3, bodySize/3);
    ellipse(-faceSize/2.5, faceSize/2, bodySize/3, bodySize/3);

    //LEGS
    stroke(furLittleLight);
    fill(fur);
    ellipse(faceSize/2, faceSize, bodySize/2, bodySize/2);
    ellipse(-faceSize/2, faceSize, bodySize/2, bodySize/2);
  }

  private void drawNeeds()
  {
    //HE IS THINGING OF SMTH
    noStroke();
    int thoughtStrength = isHungry > 0 ? (int)(255 * isHungry) : (int)(255 * isSad);
    String text = isHungry > 0 ? "I'm hungry" : "I'm sad";
    fill(255, 255, 255, thoughtStrength);
    ellipse(0, faceSize * -0.6, faceSize * 0.15, faceSize * 0.15);    
    ellipse(faceSize * 0.2, faceSize * -0.75, faceSize * 0.3, faceSize * 0.3);
    ellipse(faceSize * 0.2, faceSize * -1, faceSize * 0.1, faceSize * 0.1);    
    ellipse(0, faceSize * -1.7, faceSize * 1.2, faceSize * 1.2);

    fill(0, 0, 0, thoughtStrength);    
    textSize(faceSize * 0.2);
    textAlign(CENTER);
    textMode(SHAPE);
    text(text, 0, faceSize * -1.7);
  }

  private void drawFace()
  {
    //EARS
    noStroke();
    fill(furLittleDark);
    ellipse(faceSize/2.5, -faceSize/2.5, faceSize/3, faceSize/3);
    ellipse(-faceSize/2.5, -faceSize/2.5, faceSize/3, faceSize/3);

    //FACE
    fill(fur);
    noStroke();
    ellipse(0, 0, faceSize, faceSize);

    //MOUTH/NOSE REGION
    stroke(furLittleLight);
    fill(furLight);
    ellipse(0, mouthDistance/1.2, faceSize/2.5, faceSize/2.5);

    //EYES
    stroke(furLittleDark);
    fill(0);    
    ellipse(-eyeDistance, -face.eyeLeftHeight, eyeSize, eyeSize);
    ellipse(eyeDistance, -face.eyeRightHeight, eyeSize, eyeSize);

    //EYEBROWS    
    noFill();  
    stroke(furLittleLight);
    arc(-eyeDistance, -face.leftEyebrowHeight, eyeSize*2, eyeSize*2, PI, TWO_PI);
    arc(eyeDistance, -face.rightEyebrowHeight, eyeSize*2, eyeSize*2, PI, TWO_PI);

    //MOUTH    
    stroke(furDark);
    ellipse(0, mouthDistance, face.mouthWidth, face.mouthHeight);

    //NOSE
    stroke(0);
    fill(furDark);
    ellipse(0, face.nostrilHeight/2, noseSize, noseSize/2);
  }
}