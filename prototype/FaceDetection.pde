
import oscP5.*;
OscP5 oscP5;

class FaceDetection {
  // num faces found
  public int found;

  // eating
  public boolean currentlyEating = false;
  public int amountMouthOpened = 0;
  public boolean mouthOpenedIncreased = false;
  
  
  // pose
  public float poseScale;
  public PVector posePosition = new PVector();
  public PVector poseOrientation = new PVector();

  // gesture
  public float mouthHeight;
  public float mouthWidth;
  public float eyeLeftHeight;
  public float eyeRightHeight;
  public float leftEyebrowHeight;
  public float rightEyebrowHeight;
  public float jaw;
  public float nostrilHeight;

  public FaceDetection(OscP5 oscP5)
  {
    oscP5.plug(this, "found", "/found");
    oscP5.plug(this, "poseScale", "/pose/scale");
    oscP5.plug(this, "posePosition", "/pose/position");
    oscP5.plug(this, "poseOrientation", "/pose/orientation");
    oscP5.plug(this, "mouthWidthReceived", "/gesture/mouth/width");
    oscP5.plug(this, "mouthHeightReceived", "/gesture/mouth/height");
    oscP5.plug(this, "eyeLeftReceived", "/gesture/eye/left");
    oscP5.plug(this, "eyeRightReceived", "/gesture/eye/right");
    oscP5.plug(this, "eyebrowLeftReceived", "/gesture/eyebrow/left");
    oscP5.plug(this, "eyebrowRightReceived", "/gesture/eyebrow/right");
    oscP5.plug(this, "jawReceived", "/gesture/jaw");
    oscP5.plug(this, "nostrilsReceived", "/gesture/nostrils");
  }

  // OSC CALLBACK FUNCTIONS

  public void found(int i) {
    //println("found: " + i);
    if (found == 0) {
      found = i;
    }
  }
  
  public boolean isEating() {
    return currentlyEating;
  }
  
  public void setEating(boolean b) {
    currentlyEating = b;
  }
  
  
  public void checkEating() {
    if(mouthHeight>2) {
      if(!mouthOpenedIncreased) {
        amountMouthOpened+=1; 
        mouthOpenedIncreased = true;
        if(amountMouthOpened>=3) {
          currentlyEating = true;
          amountMouthOpened = 0;
        } else {
          currentlyEating = false;
        }
      }
    } else {
      mouthOpenedIncreased = false;
    }
  }

  public void poseScale(float s) {
    //println("scale: " + s);
    poseScale = s;
  }

  public void posePosition(float x, float y) {
    //println("pose position\tX: " + x + " Y: " + y );
    posePosition.set(x, y, 0);
  }

  public void poseOrientation(float x, float y, float z) {
    //println("pose orientation\tX: " + x + " Y: " + y + " Z: " + z);
    poseOrientation.set(x, y, z);
  }

  public void mouthWidthReceived(float w) {
    //println("mouth Width: " + w);
    mouthWidth = w;
  }

  public void mouthHeightReceived(float h) {
    //println("mouth height: " + h);
    mouthHeight = h;
  }

  public void eyeLeftReceived(float f) {
    //println("eye left: " + f);
    eyeLeftHeight = f;
  }

  public void eyeRightReceived(float f) {
    //println("eye right: " + f);
    eyeRightHeight = f;
  }

  public void eyebrowLeftReceived(float f) {
    //println("eyebrow left: " + f);
    leftEyebrowHeight = f;
  }

  public void eyebrowRightReceived(float f) {
    //println("eyebrow right: " + f);
    rightEyebrowHeight = f;
  }

  public void jawReceived(float f) {
    //println("jaw: " + f);
    jaw = f;
  }

  public void nostrilsReceived(float f) {
    //println("nostrils: " + f);
    nostrilHeight = f;
  }
  

  // all other OSC messages end up here
  void oscEvent(OscMessage m) {
    if (m.isPlugged() == false) {
      //println("UNPLUGGED: " + m);
    }
  }
}