import processing.sound.*;

public class Sound 
{
 SoundFile mySound;

  Sound(PApplet p, String filename) {
    mySound = new SoundFile(p, filename);
  }

  public void play() {
    mySound.play();
  }
  
  public void setAmp(float amp) {
    mySound.amp(amp);
  }
}